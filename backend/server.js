const app = require('./app');
const connectDatabase = require('./config/database')

const dotenv = require('dotenv');
//Handle uncaughtException
process.on('uncaughtException', err => {
    console.log(`ERROR: ${err.stack}`);
    console.log('Shutting down due to uncaught exception');
    process.exit(1)
})

//Setting up config file
dotenv.config({ path: 'backend/config/config.env' })


//Connect to database
connectDatabase();

const server = app.listen(process.env.PORT, () => {
    console.log(`Server running on PORT: ${process.env.PORT} in ${process.env.NODE_ENV} mode.`)
})

//Handle Unhandled Promise rejections
process.on('unhandledRejection', err => {
    console.log(`ERROR: ${err.stack}`);
    console.log('Shutting down due to unhandled Promise rejections');
    server.close(() => {
        process.exit(1);
    })
})